package com.testinggrounds.service;

import com.testinggrounds.repo.StudentRepo;
import com.testinggrounds.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class StudentService {

    private StudentRepo studentRepo;

    @Autowired
    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    public Student addStudent(Student student){
        student.setId(UUID.randomUUID());
        return this.studentRepo.save(student);
    }
}
