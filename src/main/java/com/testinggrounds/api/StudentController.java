package com.testinggrounds.api;

import com.testinggrounds.model.Student;
import com.testinggrounds.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("/students")
public class StudentController {

    @RequestMapping (value = "/bigboi", method = RequestMethod.GET)
    public String testMethod(){
        return "Hey there";
    }

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public Student addStudent(@RequestBody Student student){
        return studentService.addStudent(student);
    }
}
