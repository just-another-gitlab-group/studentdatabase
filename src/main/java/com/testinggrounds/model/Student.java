package com.testinggrounds.model;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Student {

    @Id
    private UUID id;
    private String fullName;
    private String userName;
    private String email;

    public Student(){

    }


    public Student(UUID id, String fullName) {
        this.id = id;
        this.fullName = fullName;

        //Set up student username and email based on initials and last name
        //TODO: add prefix increment for commonalities or duplicates (eg Sam Smith ssmith1)
        String[] str = fullName.split(" ");
        String concatName = "";
        for (int i = 0; i < str.length - 1; i++) {
            concatName = concatName.concat(String.valueOf(str[i].charAt(0)).toLowerCase());
        }
        concatName = concatName.concat(str[str.length - 1].toLowerCase());

        this.userName = concatName;
        this.email = concatName.concat("@student.fullsail.edu");
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getUserName(){
        return userName;
    }

    public String getEmail() {
        return email;
    }
}
