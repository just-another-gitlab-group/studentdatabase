package com.testinggrounds.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class StudentTest {

    private Student student;

    @BeforeEach
    public void studentTest_Arrange()
    {
        student = new Student(UUID.randomUUID(), "Joshua Brooks Shevach");
    }

    @Test
    public void getId_Test()
    {
        //Arrange
        var expected = UUID.randomUUID();
        var student = new Student(expected, "Jeff Darwin");

        //Act
        var actual = student.getId();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getFullName_Test()
    {
        //Arrange
        //var expected = "Alexandra Rood";
        //var student = new Student(UUID.randomUUID(), expected);

        //Act
        var actual = student.getFullName();

        //Assert
        assertEquals("Joshua Brooks Shevach", actual);
    }

    @Test
    public void getUserName_Test()
    {
        //Arrange
        //var student = new Student(UUID.randomUUID(), "Will Smith");

        //Act
        var actual = student.getUserName();

        //Assert
        assertEquals("jbshevach", actual);
    }

    @Test
    public void getEmail_Test()
    {
        //Arrange
        //var student = new Student(UUID.randomUUID(), "Joshua Brooks Shevach");

        //Act
        var actual = student.getEmail();

        //Assert
        assertEquals("jbshevach@student.fullsail.edu", actual);
    }
}
